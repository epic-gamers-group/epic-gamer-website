<!DOCTYPE HTML>
<!--
	Caminar by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Epic Gamers</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="#">Epic Gamers Unite <span>by EpicGamers</span></a></div>
			</header>

		<!-- Main -->
			<section id="main">
				<div class="inner">

				<!-- One -->
					<section id="one" class="wrapper style1">
						
						<div class="image fit flush">
							<img src="images/pic02.jpg" alt="" />
						</div>
						<header class="special">
						<?php
						$status = json_decode(file_get_contents('https://api.mcsrvstat.us/2/dank.epicfortnitegamers.com'));

						echo "<h1 style='font-size:38px;'>Minecraft Server Info </h1>";
						echo "<h1 style='color:green;margin-left:30px;'>" . ($status->online) ? '[Online]':'[Offline]' . "</h1>";						
						
						//Printing MC version number
						echo "<h3>Minecraft Version Number: </br>" . $status->version . "</h3>";						
						echo "</br>";
						
						//Listing all players currently in the server
						echo "<h3>Current Players in Server: " . $status->players->online . "/" . $status->players->max;
						echo "</br>";
						if (! empty($status->players->list)) {
							foreach ($status->players->list as $player) {
								echo $player.'</h3><br />';
							}
						}
						//Printing all mods in server
						echo "<h3>Active Mods:</h3>";
						echo "</br>";
						if (! empty($status->mods->raw)) {
							$i = 1;
							echo "<table>";
							echo "<tr>";
							foreach ($status->mods->raw as $mods) {
								if ($i % 6 != 0) {
									echo "<td>" . $mods . " </td>";
									$i++;
								}
								else {
									echo "</tr>";
									echo "<tr>";
									$i++;
								}
							}
							echo "</tr>";
							echo "</table>";
						}
						echo "<br />";
						?>
							<h3>The epic gamer journey begins</h3>
							<p>yes</p>
						</header>
						<div class="content">
							<p>My name is Oliver Queen. For five years, I was stranded on an island with only one goal-- Survive. Now I will fulfill my father's dying wish-- to use the list of names he left me and bring down those who are poisoning my city. To do this, I must become someone else. I must become something else.</p>
							<p>My name is Oliver Queen. After five years on a hellish island, I have come home with only one goal-- to save my city. But to do so, I can't be the killer I once was. To honor my friend's memory, I must be someone else. I must be something else</p>
						</div>
					</section>

				<!-- Two -->
					<section id="two" class="wrapper style2">
						<header>
							<h2>BOKU NO DARLING<h2>
							<p>Darrrrrrrrling</p>
						</header>
						<div class="content">
							<div class="gallery">
								<div>
									<div class="image fit flush">
										<a href="images/pic03.jpg"><img src="images/pic03.jpg" alt="" /></a>
									</div>
								</div>
								<div>
									<div class="image fit flush">
										<a href="images/pic01.jpg"><img src="images/pic01.jpg" alt="" /></a>
									</div>
								</div>
								<div>
									<div class="image fit flush">
										<a href="images/pic04.jpg"><img src="images/pic04.jpg" alt="" /></a>
									</div>
								</div>
								<div>
									<div class="image fit flush">
										<a href="images/pic05.jpg"><img src="images/pic05.jpg" alt="" /></a>
									</div>
								</div>
							</div>
						</div>
					</section>

				<!-- Three -->
					<section id="three" class="wrapper">
						<div class="spotlight">
							<div class="image flush"><img src="images/pic06.jpg" alt="" /></div>
							<div class="inner">
								<h3>First one</h3>
								<p>My name is Oliver Queen. After five years in hell, I have come home with only one goal-- to save my city. Now others have joined my crusade. To them, I'm Oliver Queen. To the rest of Starling City, I am someone else. I am something else.</p>
							</div>
						</div>
						<div class="spotlight alt">
							<div class="image flush"><img src="images/pic07.jpg" alt="" /></div>
							<div  class="inner">
								<h3>Second one</h3>
								<p>My name was Oliver Queen. For three years, I worked to save my city. But to save my sister, I had to become someone else. I had to become something else.</p>
							</div>
						</div>
						<div class="spotlight">
							<div class="image flush"><img src="images/pic08.jpg" alt="" /></div>
							<div class="inner">
								<h3>Third one</h3>
								<p>My name is Oliver Queen. After five years in hell, I returned home with only one goal-- to save my city. But my old approach wasn't enough. I had to become someone else. I had to become something else. I had to become the Green Arrow.</p>
							</div>
						</div>
					</section>

				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved. Images <a href="https://unsplash.com">Unsplash</a> Design <a href="https://templated.co">TEMPLATED</a>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>